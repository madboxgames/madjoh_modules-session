define([
	'require',
	'madjoh_modules/custom_events/custom_events',
	'madjoh_modules/cache/data_cache'
],
function(require, CustomEvents, DataCache){
	var Session = {
		init : function(settings){
			Session.settings 	= settings;
			Session.bundle 		= settings.bundle;

			var sessionJson = localStorage.getItem(Session.bundle + '.session');
			document.session = sessionJson ? JSON.parse(sessionJson) : null;
			var isOpen = Session.isOpen();
			if(isOpen) CustomEvents.fireCustomEvent(document, 'LoggedIn');
			return isOpen;
		},

		open : function(sessionParams){
			var session = {
				id 		: sessionParams.id,
				token 	: sessionParams.token
			};
			document.session = session;
			localStorage.setItem(Session.bundle + '.session', JSON.stringify(session));

			CustomEvents.fireCustomEvent(document, 'LoggedIn');
		},
		close : function(stack){
			CustomEvents.fireCustomEvent(document, 'LoggingOut');
			localStorage.removeItem(Session.bundle + '.session');
			document.session = null;
			DataCache.clearAll();
			Session.settings.onLoggedOut(stack);
		},

		update : function(session){
			Session.open(session);
		},

		isOpen : function(){
			return (document.session !== null);
		},
		getKeys : function(){
			if(!Session.isOpen()) return console.log('You need to be connected');
			return document.session;
		}
	};

	return Session;
});